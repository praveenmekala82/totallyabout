package in.totallyabout.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import in.totallyabout.constants.Url;
import in.totallyabout.dto.ImagesDTO;
import in.totallyabout.dto.NewsDTO;
import in.totallyabout.dto.NewsResponseDTO;
import in.totallyabout.utility.HttpNewsClient;

@RestController
public class ImagesController {
	HttpNewsClient httpClient;

	@PostConstruct
	private void init() {
		httpClient = new HttpNewsClient();
	}
	
	@RequestMapping(method = RequestMethod.PUT, value = "/images/bulkcreate")
	@ResponseBody
	public String createBulkImages(@RequestBody List<ImagesDTO> bulkImagesDTO) {
		String response = "failed";
		for (int i = 0; i < bulkImagesDTO.size(); i++) {
			System.out.println(bulkImagesDTO.get(i).toString());
			response = httpClient.putImages(bulkImagesDTO.get(i));
		}
		return response;
	}
	
	@RequestMapping(method = RequestMethod.GET, value = "/images/{celebrityId}")
	@ResponseBody
	public List<ImagesDTO> getImages(@PathVariable String celebrityId,
			@RequestParam(required = false) Integer page,
			@RequestParam(required = false) Integer pageSize
			) throws UnsupportedEncodingException {
		if(page == null) page = 1;
		if(pageSize == null) pageSize = 10;
		int from = (page-1)*pageSize;
		int size = pageSize;

		String url = Url.ES + "/images/_search?from="+from+"&size="+size+"&q="
				+ URLEncoder.encode("celebId:" + celebrityId , "UTF-8");
		List<ImagesDTO> res = new ArrayList<ImagesDTO>();
		try {

			res = httpClient.getImagesResponse(url);
		} catch (JsonProcessingException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
		return res;

	}
	
}
