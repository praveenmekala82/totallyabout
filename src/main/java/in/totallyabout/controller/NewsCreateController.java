package in.totallyabout.controller;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import in.totallyabout.dto.NewsDTO;
import in.totallyabout.utility.HttpNewsClient;

@RestController
public class NewsCreateController {

	public final static Logger logger = Logger.getLogger(NewsCreateController.class);

	@Autowired
	CelebrityReadController celebrityController;
	@Autowired
	HttpNewsClient httpClient;
	
	@RequestMapping(method = RequestMethod.PUT, value = "/news/create")
	@ResponseBody
	public String createNews(@RequestBody NewsDTO newsDTO) {
		String response = httpClient.putNews(newsDTO);
		return response;
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/news/bulkcreate")
	@ResponseBody
	public String createBulkNews(@RequestBody List<NewsDTO> bulkNewsDTO) {
		String response = "failed";
		for (int i = 0; i < bulkNewsDTO.size(); i++) {
			//logger.info(bulkNewsDTO.get(i).toString());
			response = httpClient.putNews(bulkNewsDTO.get(i));
		}
		return response;
	}
}
