package in.totallyabout.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import in.totallyabout.constants.TotallyAboutConstants;
import in.totallyabout.dao.CelebrityDAO;
import in.totallyabout.dto.CelebrityInfoRequestDTO;
import in.totallyabout.entity.CelebrityInfo;
import in.totallyabout.entity.CelebrityUniqueNews;
import in.totallyabout.entity.NewsIds;
import in.totallyabout.utility.DTOPopulator;
import in.totallyabout.utility.HttpNewsClient;

@RestController
public class CelebrityWriteController {
	@Autowired
	private MongoTemplate mongoTemplate;

	@Autowired
	HttpNewsClient httpClient;
	
	public final static Logger logger = Logger.getLogger(CelebrityReadController.class);
	@RequestMapping(method = RequestMethod.PUT, value = "/celebrity/create", produces = "application/json")
	@ResponseBody
	public void createCelebrity(@RequestBody CelebrityInfo celebrityInfo, @RequestParam Integer rating) {
		logger.info("creating celebrity " + celebrityInfo.getDisplayName());
		if(celebrityInfo.getId()!=null){
			mongoTemplate.insert(celebrityInfo, TotallyAboutConstants.CELEBRITY_INFO);
			httpClient.putESCelebData(celebrityInfo, rating);
			CelebrityUniqueNews celebrityUniqueNews = new CelebrityUniqueNews();
			celebrityUniqueNews.set_id(celebrityInfo.getId());
			List<NewsIds> newsIds = new ArrayList<NewsIds>();
			celebrityUniqueNews.setNewsIds(newsIds);
			CelebrityDAO.createCelebrityUniqueNews(celebrityUniqueNews);
		}else{
			logger.info("Creating celebrity failed with id..."+celebrityInfo.getId());
		}
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/celebrity/bulkcreate", produces = "application/json")
	@ResponseBody
	public void createCelebrities(@RequestBody List<CelebrityInfoRequestDTO> celebrityInfoRequestDTO) {
		logger.info("In create Celebrities bulk api");
		logger.info("size " + celebrityInfoRequestDTO.size());
		CelebrityUniqueNews celebrityUniqueNews = new CelebrityUniqueNews();
		for (int i = 0; i < celebrityInfoRequestDTO.size(); i++) {
			if (celebrityInfoRequestDTO.get(i) != null) {
				CelebrityInfo celebrityInfo = DTOPopulator.celebrityDTOPopulator(celebrityInfoRequestDTO.get(i));
				logger.info("creating celebrity " + celebrityInfo.getDisplayName());
				mongoTemplate.insert(celebrityInfo, TotallyAboutConstants.CELEBRITY_INFO);
				httpClient.putESCelebData(celebrityInfo, celebrityInfoRequestDTO.get(i).getRating());
				celebrityUniqueNews.set_id(celebrityInfoRequestDTO.get(i).getId());
				List<NewsIds> newsIds = new ArrayList<NewsIds>();
				celebrityUniqueNews.setNewsIds(newsIds);
				CelebrityDAO.createCelebrityUniqueNews(celebrityUniqueNews);
			}
		}
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/celebrity/bulkupdate", produces = "application/json")
	@ResponseBody
	public void updateCelebrity(@RequestBody List<CelebrityInfoRequestDTO> celebrityInfoRequestDTO) {
		logger.info("In update Celebrities bulk api");
		logger.info("size " + celebrityInfoRequestDTO.size());

		for (int i = 0; i < celebrityInfoRequestDTO.size(); i++) {
			if (celebrityInfoRequestDTO.get(i) != null) {
				CelebrityInfo celebrityInfo = DTOPopulator.celebrityDTOPopulator(celebrityInfoRequestDTO.get(i));
				Query query = new Query(Criteria.where("_id").is(celebrityInfo.getId()));
				Update update = new Update();
				DBObject dbDoc = new BasicDBObject();
				// it is the one spring use for conversions
				mongoTemplate.getConverter().write(celebrityInfo, dbDoc); 
				fromDBObjectExcludeNullFields(dbDoc, update, "");
				mongoTemplate.upsert(query, update, TotallyAboutConstants.CELEBRITY_INFO);// save(celebrityInfo,CELEBRITY_INFO);
				Boolean updateESNameAndRating = celebrityInfoRequestDTO.get(i).getUpdateESNameAndRating();
				int rating = celebrityInfoRequestDTO.get(i).getRating();
				if (updateESNameAndRating != null && updateESNameAndRating) {
					httpClient.putESCelebData(celebrityInfo, rating);
				}
			}
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/celebrity/update", produces = "application/json")
	@ResponseBody
	public void updateCelebrity(@RequestBody CelebrityInfo celebrityInfo, @RequestParam Integer rating, @RequestParam Boolean updateESNameAndRating) {
		logger.info("In updateCelebrity controller");
		if (celebrityInfo.getId() != null) {
			Query query = new Query(Criteria.where("_id").is(celebrityInfo.getId()));
			Update update = new Update();
			DBObject dbDoc = new BasicDBObject();
			// it is the one spring use for conversions
			mongoTemplate.getConverter().write(celebrityInfo, dbDoc);
			fromDBObjectExcludeNullFields(dbDoc, update, "");
			mongoTemplate.upsert(query, update, TotallyAboutConstants.CELEBRITY_INFO);// save(celebrityInfo,CELEBRITY_INFO);
			if (updateESNameAndRating != null && updateESNameAndRating) {
				httpClient.putESCelebData(celebrityInfo, rating);
			}
		} else {
			logger.info("Updating celebrity failed with id..." + celebrityInfo.getId());
		}
	}

	@RequestMapping(method = RequestMethod.DELETE, value = "/celebrity/{id}")
	@ResponseBody
	public void deleteCeleb(@PathVariable("id") String id) {
		Query query = new Query(Criteria.where("_id").is(id));
		mongoTemplate.remove(query, CelebrityInfo.class, TotallyAboutConstants.CELEBRITY_INFO);
	}
	

	public Update fromDBObjectExcludeNullFields(DBObject object, Update update, String parent) {
		logger.info("In fromDBObjectExcludeNullFields");
		for (String key : object.keySet()) {
			Object value = object.get(key);
			if (value != null) {
				if (!(value instanceof String || value instanceof Collection)) {
					logger.info("not string, not collection: " + value);
					DBObject dbDoc = new BasicDBObject();
					mongoTemplate.getConverter().write(value, dbDoc);
					fromDBObjectExcludeNullFields(dbDoc, update, parent + key + ".");
				} else {
					logger.info(" may be string or collection: " + value);
					update.set(parent + key, value);
				}
			}
		}
		return update;
	}


}
