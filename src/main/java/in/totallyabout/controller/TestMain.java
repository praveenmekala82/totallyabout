package in.totallyabout.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class TestMain {

	public static void main(String[] args) throws Exception {
		BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
		System.out.print("Enter your name: ");
		 
		String name = reader.readLine();
		System.out.println("Your name is: " + name);
	}
	
	//2,3,10,2,4,8,1
	
	static int maxDifference(int[] a) {
		int size = a.length;
		int difference = -1;
		int minimumElement = a[0];
		
		
		for(int i=1;i<size;i++) {
			int differenceNew = a[i] - minimumElement;
			if(differenceNew > difference) {
				difference = differenceNew;
			}
			if(a[i] < minimumElement) {
				minimumElement = a[i];
			}
		}
		return difference;
	}
}
