package in.totallyabout.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.InetAddress;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.apache.log4j.Logger;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.search.SearchType;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.settings.Settings;
import org.elasticsearch.common.transport.TransportAddress;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.transport.client.PreBuiltTransportClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.http.MediaType;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;

import in.totallyabout.constants.TotallyAboutConstants;
import in.totallyabout.constants.Url;
import in.totallyabout.dto.CelebAutocompleteDTO;
import in.totallyabout.dto.NewsDTO;
import in.totallyabout.dto.NewsResponseDTO;
import in.totallyabout.entity.CelebrityUniqueNews;
import in.totallyabout.entity.NewsIds;
import in.totallyabout.redis.RedisHelper;
import in.totallyabout.utility.HttpNewsClient;

import javax.annotation.PostConstruct;

@RestController
public class NewsController {

	@Autowired
	CelebrityReadController celebrityController;
	@Autowired
	private MongoTemplate mongoTemplate;
	@Autowired
	private RedisTemplate<String, Object> redisTemplate;
	@Autowired
	private RedisHelper redisHelper;
	@Autowired
	HttpNewsClient httpClient;
	TransportClient client;

	@PostConstruct
	private void init() throws Exception {
		client = new PreBuiltTransportClient(Settings.EMPTY).
				addTransportAddress(new TransportAddress(InetAddress.getByName("192.168.0.11"), 9300));
	}

	public final static Logger logger = Logger.getLogger(NewsController.class);
	@RequestMapping(method = RequestMethod.POST, value = "/news/search/")
	@ResponseBody
	public String getNews(@RequestBody NewsDTO newsDTO) {
		String url = Url.ES + "/" + newsDTO.getHandleSource() + "/" + newsDTO.getHandleId()
		+ "?pretty&filter_path=_source";
		String response = httpClient.getNews(url);
		return response;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/news/{celebrityId}")
	@ResponseBody
	public NewsResponseDTO getCelebIndirectNewsByRangeAndSearchTermsNew(
																	 @PathVariable String celebrityId, @RequestParam(required = false, defaultValue = "ALL") String handleType,
																	 @RequestParam(required = false) String handleSource, @RequestParam(required = false) String searchTerms,
																	 @RequestParam(required = false) Integer page, @RequestParam(required = false) Integer pageSize)
			throws Exception {
		NewsResponseDTO newsresDTO = new NewsResponseDTO();
		if (page == null)
			page = 1;
		if (pageSize == null)
			pageSize = 10;
		int from = (page - 1) * pageSize;
		int size = pageSize;

		SearchResponse searchResponse = client.prepareSearch("news").setTypes("handleNews").setSearchType(SearchType.QUERY_THEN_FETCH).setQuery(QueryBuilders.boolQuery()
				.filter(QueryBuilders.termQuery("celebrities", celebrityId))).setFrom(from).setSize(size).setExplain(true).get();
		System.out.println("response" + searchResponse);
		JsonParser parser = new JsonParser();
		System.out.println(searchResponse.toString());
		JsonObject json = (JsonObject) parser.parse(searchResponse.toString());
		JsonObject obj = json.get("hits").getAsJsonObject();

		JsonArray jarray = obj.get("hits").getAsJsonArray();
		List<NewsDTO> news = new ArrayList<NewsDTO>();

		for (int i = 0; i < jarray.size(); i++) {

			NewsDTO newsdto = new NewsDTO();

			JsonObject ids = jarray.get(i).getAsJsonObject();
			JsonObject source = ids.get("_source").getAsJsonObject();
			newsdto = new Gson().fromJson(source.toString(), NewsDTO.class);

			newsdto.setHandleSource(ids.get("_type").getAsString());
			newsdto.setNewsId(ids.get("_id").getAsString());

			news.add(newsdto);

		}
		newsresDTO.setIndirect(news);
		return newsresDTO;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/news/{celebrityId}",consumes = "text/plain")
	@ResponseBody
	public NewsResponseDTO getCelebIndirectNewsByRangeAndSearchTerms(@RequestBody String query,
			@PathVariable String celebrityId, @RequestParam(required = false, defaultValue = "ALL") String handleType,
			@RequestParam(required = false) String handleSource, @RequestParam(required = false) String searchTerms,
			@RequestParam(required = false) Integer page, @RequestParam(required = false) Integer pageSize)
					throws UnsupportedEncodingException {
		// httpClient = new HttpNewsClient();
		// String url = Url.ES + "/" + newsDTO.getHandleSource() + "/" +
		// newsDTO.getHandleId()+ "?pretty&filter_path=_source";
		// System.out.println(url);
		// String response = httpClient.getNews(url);
		NewsResponseDTO newsresDTO = new NewsResponseDTO();
		ValueOperations<String, Object> ops = this.redisTemplate.opsForValue();

		// String key = String.format( "user:%s", celebrityId);

		String key = "news:" + celebrityId + ":" + (handleType == null ? "all" : handleType) + ":"
				+ (handleSource == null ? "all" : handleSource) + ":" + (searchTerms == null ? "none" : searchTerms)
				+ ":" + (page == null ? 1 : page) + ":" + (pageSize == null ? 10 : pageSize);

		if (page == null)
			page = 1;
		if (pageSize == null)
			pageSize = 10;
		int from = (page - 1) * pageSize;
		int size = pageSize;

		if (!handleType.equalsIgnoreCase(("Direct"))) {

			if (StringUtils.isEmpty(searchTerms)) {
				searchTerms = celebrityController.getCelebritySearchTerms(celebrityId);

				if (StringUtils.isEmpty(searchTerms))
					searchTerms = celebrityController.getCelebrityDisplayName(celebrityId);

				/*String url1 = Url.ES + "/news/_search?from=" + from + "&sort=postedAt:desc&size=" + size + "&q="
						+ URLEncoder.encode(
								"title:" + searchTerms + " AND description:" + searchTerms + " AND handleType:Indirect",
								"UTF-8"); */
				String url1= Url.ES + "/news/_search";

				/*if (!(StringUtils.isEmpty(handleSource))) {
					url1 = url1 + URLEncoder.encode(" AND _type:" + handleSource, "UTF-8");

				}*/
				if(query.contains("descriptionText"))
					query = query.replace("descriptionText", searchTerms);
				List<NewsDTO> res = httpClient.getNewsbyRequestBody(url1, query);
				newsresDTO.setIndirect(res);
				redisHelper.saveRedisKey(key, newsresDTO,TotallyAboutConstants.NEWS_CACHE_TTL);
			}
		} else {
			newsresDTO = null;
		}
		return newsresDTO;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/celebrity/autocomplete")
	@ResponseBody
	public List<CelebAutocompleteDTO> getResults(@RequestParam String size, @RequestParam String name) {
		logger.info("in getResults");
		// String url =
		// "http://auth01.dev.insideview.com:9200/news/"+newsDTO.getSource()+"/"+newsDTO.getNewsId()+"/";
		String url = Url.ES + "/celebrity/_search?size=" + size + "&q=name:" + name + "*";
		System.out.println(url);
		return httpClient.getAutocompleteResponse(url);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/news/v1/{celebrityId}")
	@ResponseBody
	public NewsResponseDTO getNews(@PathVariable String celebrityId,
			@RequestParam(required = false, defaultValue = "ALL") String handleType,
			@RequestParam(required = false) String handleSource, @RequestParam(required = false) String searchTerms,
			@RequestParam(required = false) Integer page, @RequestParam(required = false) Integer pageSize)
					throws UnsupportedEncodingException {
		NewsResponseDTO newsresDTO = new NewsResponseDTO();

		String key = "news:" + celebrityId + ":" + (handleType == null ? "all" : handleType) + ":"
				+ (handleSource == null ? "all" : handleSource) + ":" + (searchTerms == null ? "none" : searchTerms)
				+ ":" + (page == null ? 1 : page) + ":" + (pageSize == null ? 10 : pageSize);

		if (redisHelper.hasKey(key)) {
			return redisHelper.getValue(key, NewsResponseDTO.class);
		}
		
		if (page == null)
			page = 1;
		if (pageSize == null)
			pageSize = 10;
		int from = (page - 1) * pageSize;
		int size = pageSize;
		String url;
		if (!handleType.equalsIgnoreCase(("InDirect"))) {
			url = Url.ES + "/news/_search?from=" + from + "&size=" + size + "&sort=postedAt:desc&q="
					+ URLEncoder.encode("celebrityId:" + celebrityId + " AND handleType:Direct", "UTF-8");
			if (!(StringUtils.isEmpty(handleSource))) {
				url = url + URLEncoder.encode(" AND _type:" + handleSource, "UTF-8");
			}
			try {
				List<NewsDTO> res;
				res = httpClient.getNewsResponse(url);
				newsresDTO.setDirect(res);
			} catch (JsonProcessingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}

		if (!handleType.equalsIgnoreCase(("Direct"))) {
			List<NewsDTO> indirectNews = new ArrayList<NewsDTO>();
			newsresDTO.setIndirect(indirectNews);
			CelebrityUniqueNews currentCelebrityUniqueNews = mongoTemplate.findOne(
					Query.query(Criteria.where("_id").is(celebrityId)), CelebrityUniqueNews.class,
					TotallyAboutConstants.CELEBRITY_UNIQUE_NEWS);
			if(currentCelebrityUniqueNews == null || CollectionUtils.isEmpty(currentCelebrityUniqueNews.getNewsIds())) {
				return redisHelper.saveRedisKey(key, newsresDTO,TotallyAboutConstants.NEWS_CACHE_TTL);
			}
			List<NewsIds> newsIds = currentCelebrityUniqueNews.getNewsIds();
			try {
				//newsIds = newsIds.subList(Integer.min(((page==null?1:page)-1)*pageSize, newsIds.size()), Integer.min((page==null?1:page)*pageSize, newsIds.size())) ;
			} catch (IndexOutOfBoundsException e) {
				logger.info("out of bound exception");
				return redisHelper.saveRedisKey(key, newsresDTO,TotallyAboutConstants.NEWS_CACHE_TTL);
			}
			if(newsIds.isEmpty()) {
				return redisHelper.saveRedisKey(key, newsresDTO,TotallyAboutConstants.NEWS_CACHE_TTL);
			}
			url = Url.ES + "/news/_search";
			StringBuilder query =  new StringBuilder();
			query.append("{\"query\":{\"ids\":{\"values\":[");
			for(int i=0;i<(newsIds.size()-1);i++) {
				query.append("\""+newsIds.get(i).getNewsId()+"\",");
			}
			query.append("\""+newsIds.get(newsIds.size()-1).getNewsId()+"\"");
			query.append("]}}}");
			List<NewsDTO> res = httpClient.getNewsbyRequestBody(url, query.toString());
			newsresDTO.setIndirect(res);
		}
		return redisHelper.saveRedisKey(key, newsresDTO,TotallyAboutConstants.NEWS_CACHE_TTL);
	}



	@RequestMapping(method = RequestMethod.GET, value = "/v0/news/{celebrityId}")
	@ResponseBody
	// @Cacheable("celebrity")
	public NewsResponseDTO getNewsV0(@PathVariable String celebrityId,
			@RequestParam(required = false, defaultValue = "ALL") String handleType,
			@RequestParam(required = false) String handleSource, @RequestParam(required = false) String searchTerms,
			@RequestParam(required = false) Integer page, @RequestParam(required = false) Integer pageSize)
					throws UnsupportedEncodingException {

		NewsResponseDTO newsresDTO = new NewsResponseDTO();
		ValueOperations<String, Object> ops = this.redisTemplate.opsForValue();

		// String key = String.format( "user:%s", celebrityId);

		String key = "news:" + celebrityId + ":" + (handleType == null ? "all" : handleType) + ":"
				+ (handleSource == null ? "all" : handleSource) + ":" + (searchTerms == null ? "none" : searchTerms)
				+ ":" + (page == null ? 1 : page) + ":" + (pageSize == null ? 10 : pageSize);

		if (redisTemplate.hasKey(key)) {
			newsresDTO = (NewsResponseDTO) ops.get(key);
			// newsresDTO =
			// (NewsResponseDTO)redisTemplate.opsForHash().get(celebrityId,
			// newsresDTO.hashCode());
		} else {

			if (page == null)
				page = 1;
			if (pageSize == null)
				pageSize = 10;
			int from = (page - 1) * pageSize;
			int size = pageSize;
			String url;

			if (!handleType.equalsIgnoreCase(("InDirect"))) {
				url = Url.ES + "/news/_search?from=" + from + "&size=" + size + "&sort=postedAt:desc&q="
						+ URLEncoder.encode("celebrityId:" + celebrityId + " AND handleType:Direct", "UTF-8");
				if (!(StringUtils.isEmpty(handleSource))) {
					url = url + URLEncoder.encode(" AND _type:" + handleSource, "UTF-8");

				}
				System.out.println("URL: " + url);

				try {
					List<NewsDTO> res;
					res = httpClient.getNewsResponse(url);
					newsresDTO.setDirect(res);

					// Map<String,Object> newsDTOMap = new HashMap<String,
					// Object>();
					// newsDTOMap.put("Indirect",newsresDTO.getIndirect());
					// newsDTOMap.put("Direct",newsresDTO.getDirect());

					redisHelper.saveRedisKey(key, newsresDTO,TotallyAboutConstants.NEWS_CACHE_TTL);

				} catch (JsonProcessingException e) {

					e.printStackTrace();
				} catch (IOException e) {

					e.printStackTrace();
				}
			}

			if (!handleType.equalsIgnoreCase(("Direct"))) {

				if (StringUtils.isEmpty(searchTerms)) {
					searchTerms = celebrityController.getCelebritySearchTerms(celebrityId);

					if (StringUtils.isEmpty(searchTerms))
						searchTerms = celebrityController.getCelebrityDisplayName(celebrityId);

					String url1 = Url.ES + "/news/_search?from=" + from + "&sort=postedAt:desc&size=" + size + "&q="
							+ URLEncoder.encode("title:" + searchTerms + " AND description:" + searchTerms
									+ " AND handleType:Indirect", "UTF-8");

					if (!(StringUtils.isEmpty(handleSource))) {
						url1 = url1 + URLEncoder.encode(" AND _type:" + handleSource, "UTF-8");

					}
					try {

						List<NewsDTO> res = httpClient.getNewsResponse(url1);
						newsresDTO.setIndirect(res);
						redisHelper.saveRedisKey(key, newsresDTO,TotallyAboutConstants.NEWS_CACHE_TTL);

					} catch (UnsupportedEncodingException e) {

						e.printStackTrace();
					} catch (JsonProcessingException e) {

						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return newsresDTO;
	}

	// @RequestMapping(method = RequestMethod.GET, value =
	// "/news/{celebrityId}")
	// @ResponseBody
	// @Cacheable("celebrity")
	public NewsResponseDTO getNewsCopy(@PathVariable String celebrityId,
			@RequestParam(required = false, defaultValue = "ALL") String handleType,
			@RequestParam(required = false) String handleSource, @RequestParam(required = false) String searchTerms,
			@RequestParam(required = false) Integer page, @RequestParam(required = false) Integer pageSize,
			@RequestParam(required = false) String postedAt) throws UnsupportedEncodingException {

		NewsResponseDTO newsresDTO = new NewsResponseDTO();
		ValueOperations<String, Object> ops = this.redisTemplate.opsForValue();

		// String key = String.format( "user:%s", celebrityId);

		String key = "news:" + celebrityId + ":" + (handleType == null ? "all" : handleType) + ":"
				+ (handleSource == null ? "all" : handleSource) + ":" + (searchTerms == null ? "none" : searchTerms)
				+ ":" + (page == null ? 1 : page) + ":" + (pageSize == null ? 10 : pageSize);

		if (redisTemplate.hasKey(key)) {
			newsresDTO = (NewsResponseDTO) ops.get(key);
			// newsresDTO =
			// (NewsResponseDTO)redisTemplate.opsForHash().get(celebrityId,
			// newsresDTO.hashCode());
		} else {

			if (page == null)
				page = 1;
			if (pageSize == null)
				pageSize = 10;
			int from = (page - 1) * pageSize;
			int size = pageSize;
			String url;

			if (!handleType.equalsIgnoreCase(("InDirect"))) {
				url = Url.ES + "/news/_search?from=" + from + "&size=" + size + "&sort=postedAt:desc&q="
						+ URLEncoder.encode("celebrityId:" + celebrityId + " AND handleType:Direct", "UTF-8");
				if (!(StringUtils.isEmpty(handleSource))) {
					url = url + URLEncoder.encode(" AND _type:" + handleSource, "UTF-8");

				}
				System.out.println("URL: " + url);

				try {
					List<NewsDTO> res;
					res = httpClient.getNewsResponse(url);
					newsresDTO.setDirect(res);

					// Map<String,Object> newsDTOMap = new HashMap<String,
					// Object>();
					// newsDTOMap.put("Indirect",newsresDTO.getIndirect());
					// newsDTOMap.put("Direct",newsresDTO.getDirect());

					redisHelper.saveRedisKey(key, newsresDTO,TotallyAboutConstants.NEWS_CACHE_TTL);

				} catch (JsonProcessingException e) {

					e.printStackTrace();
				} catch (IOException e) {

					e.printStackTrace();
				}
			}

			if (!handleType.equalsIgnoreCase(("Direct"))) {

				if (StringUtils.isEmpty(searchTerms)) {
					searchTerms = celebrityController.getCelebritySearchTerms(celebrityId);

					String url1 = Url.ES + "/news/_search?from=" + from + "&sort=postedAt:desc&size=" + size + "&q="
							+ URLEncoder.encode("title:" + searchTerms + " AND description:" + searchTerms
									+ " AND handleType:Indirect", "UTF-8");
					if (!(StringUtils.isEmpty(handleSource))) {
						url1 = url1 + URLEncoder.encode(" AND _type:" + handleSource, "UTF-8");

					}
					try {

						List<NewsDTO> res = httpClient.getNewsResponse(url1);
						newsresDTO.setIndirect(res);
						redisHelper.saveRedisKey(key, newsresDTO,TotallyAboutConstants.NEWS_CACHE_TTL);

					} catch (UnsupportedEncodingException e) {

						e.printStackTrace();
					} catch (JsonProcessingException e) {

						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
				}
			}
		}
		return newsresDTO;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/news/all")
	@ResponseBody
	// @Cacheable("allNews")
	public NewsResponseDTO getAllNews(@RequestParam(required = false, defaultValue = "ALL") String handleType,
			@RequestParam(required = false) String handleSource, @RequestParam(required = false) Integer page,
			@RequestParam(required = false) Integer pageSize

			) throws UnsupportedEncodingException {
		if (page == null)
			page = 1;
		if (pageSize == null)
			pageSize = 10;
		int from = (page - 1) * pageSize;
		int size = pageSize;

		final String key = "news:all:" + (handleType == null ? "all" : handleType) + ":"
				+ (handleSource == null ? "all" : handleSource) + ":" + (page == null ? 1 : page) + ":"
				+ (pageSize == null ? 10 : pageSize);

		NewsResponseDTO newsresDTO = new NewsResponseDTO();
		ValueOperations<String, Object> ops = this.redisTemplate.opsForValue();

		if (this.redisTemplate.hasKey(key)) {
			System.out.println("in allews redis cache");
			return (NewsResponseDTO) ops.get(key);
		} else {

			System.out.println("Not in allews redis cache");
			if (!handleType.equalsIgnoreCase(("InDirect"))) {
				String url = Url.ES + "/news/_search?from=" + from + "&size=" + size + "&sort=postedAt:desc&q="
						+ URLEncoder.encode("handleType:Direct", "UTF-8");
				if (!(StringUtils.isEmpty(handleSource))) {
					url = url + URLEncoder.encode(" AND _type:" + handleSource, "UTF-8");

				}
				try {
					List<NewsDTO> res;
					res = httpClient.getNewsResponse(url);
					newsresDTO.setDirect(res);
					ops.set(key, newsresDTO);
					this.redisTemplate.expire(key, 2, TimeUnit.MINUTES);

				} catch (JsonProcessingException e) {
					e.printStackTrace();
				} catch (IOException e) {

					e.printStackTrace();
				}
			}

			if (!handleType.equalsIgnoreCase(("Direct"))) {
				String url = Url.ES + "/news/_search?from=" + from + "&sort=postedAt:desc&size=" + size + "&q="
						+ URLEncoder.encode("handleType:Indirect", "UTF-8");
				if (!(StringUtils.isEmpty(handleSource))) {
					url = url + URLEncoder.encode(" AND _type:" + handleSource, "UTF-8");
				}
				try {
					String handleIds = "hindustantimesports,hindustantimescricket,hindustantimesfootball,hindustantimesTennis,hindustantimesOthersports,hindustantimesEntertainment,hindustantimesbolywod,hindustantimesHollywood";

					System.out.println("url is : " + url);
					List<NewsDTO> res = httpClient.getNewsResponse(url);
					newsresDTO.setIndirect(res);
					ops.set(key, newsresDTO);
					this.redisTemplate.expire(key, 2, TimeUnit.MINUTES);

				} catch (UnsupportedEncodingException e) {

					e.printStackTrace();
				} catch (JsonProcessingException e) {

					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			return newsresDTO;
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/news")
	@ResponseBody
	public NewsDTO getNewsbyNewsId(@RequestParam String newsId) throws UnsupportedEncodingException {

		NewsResponseDTO newsresDTO = new NewsResponseDTO();
		List<NewsDTO> indirectNews = null;
		ValueOperations<String, Object> ops = this.redisTemplate.opsForValue();

		// String key = String.format( "user:%s", celebrityId);

		String key = newsId;

		if (redisTemplate.hasKey(key)) {
			newsresDTO = (NewsResponseDTO) ops.get(key);
			// newsresDTO =
			// (NewsResponseDTO)redisTemplate.opsForHash().get(celebrityId,
			// newsresDTO.hashCode());
		} else {

			String url;
			url = Url.ES + "/news/_search?q=_id:" + newsId.trim();

			if (newsId.contains("-"))
				url = Url.ES + "/news/_search?q=" + URLEncoder.encode("_id:\\" + newsId, "UTF-8");

			try {
				indirectNews = httpClient.getNewsResponse(url);
				// newsresDTO.setIndirect(res);

			} catch (JsonProcessingException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return indirectNews.get(0);
	}
}
