package in.totallyabout.controller;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import in.totallyabout.dto.CelebrityNewsDTO;
import in.totallyabout.utility.CelebrityNewsUtility;

@RestController
public class CelebrityNewsController {
	
	@Autowired
	CelebrityNewsUtility celebrityNewsUtility;


	@RequestMapping( method=RequestMethod.GET, value="/celebritynews/topsearch",produces = "application/json")
	@ResponseBody
	public List<CelebrityNewsDTO> getTopSearchNewsByCategory(
			@RequestParam(value="category",required = false) String categoryName,
			@RequestParam String id) throws Exception {
        return celebrityNewsUtility.getTopSearchNewsByCategory(categoryName,id);
	}

}
