package in.totallyabout.controller;


import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mongodb.DB;
import com.mongodb.MongoClient;

import in.totallyabout.constants.Url;
import in.totallyabout.utility.CelebrityNewsUtility;
import in.totallyabout.utility.HttpNewsClient;
import redis.clients.jedis.Jedis;

@RestController
public class HealthCheckController {
	
	@Value("${mongo.host-name}")
	public String mongoHostName;
	
	@Value("${mongo.port}")
	public int mongoPort;
	
	@Value("${redis.host-name}")
	public String redisHostName;
	
	@Value("${server.url}")
	public String serverUrl;
	
	
	@Autowired
	private CelebrityNewsUtility celebrityNewsUtility;
	@Autowired
	private HttpNewsClient httpNewsClient;
	
	public final static Logger logger = Logger.getLogger(HealthCheckController.class);

	
	@RequestMapping(method=RequestMethod.GET, value="/healthcheck/mongo")
	@ResponseBody
	public String mongoHealthCheck() {
		logger.info("in mongoHealthCheck");	
		JSONObject response = new JSONObject();		
		try {
			//MongoClient mongoClient = new MongoClient("localhost",27017);
			MongoClient mongoClient = new MongoClient(mongoHostName,mongoPort);
			DB db = mongoClient.getDB( "crud" );
			db.getStats();
			response.put("status", "1");
			response.put("message", "success");
		} catch (Exception e) {
			logger.info(e.getMessage());
			response.put("status", "0");
			response.put("message", e.getMessage());
		}
		return response.toString();
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/healthcheck/es")
	@ResponseBody
	public String elassticSearchHealthCheck() {
		logger.info("in elassticSearchHealthCheck");	
		String response =null;
		
		try {
			response = httpNewsClient.getNews(Url.ES);
		} catch (Exception e) {
			logger.info(e.getMessage());
			response = e.getMessage();
		}
	  return response;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/healthcheck/redis")
	@ResponseBody
	public String redisHealthCheck() {
		logger.info("in redisHealthCheck");	
		Jedis jedis = new Jedis(redisHostName);
		String response;
		try{
		 response = jedis.ping();
		}catch(Exception e){
			response = e.getMessage();
		}
		logger.info(response);
		jedis.close();
		return response;
	}
	
	@RequestMapping(method=RequestMethod.GET, value="/healthcheck/threads")
	@ResponseBody
	public String threadsHealthCheck() {
		StringBuilder builder = new StringBuilder();
		builder.append("HttpNewsClient:");
		builder.append(httpNewsClient.getConnectionManager().getParams().getMaxTotalConnections());
		builder.append(",celebrityNewsUtility:");
		builder.append(celebrityNewsUtility.getConnectionManager().getParams().getMaxTotalConnections());
		return builder.toString();
	}
	
	/*@RequestMapping(method=RequestMethod.GET, value="/ta/healthcheck")
	public ResponseEntity<Object> appHealthCheck(HttpServletRequest request, HttpServletResponse response) {
		return new ResponseEntity<Object>("Success", HttpStatus.OK);
	}*/
}
