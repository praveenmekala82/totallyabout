package in.totallyabout.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import in.totallyabout.constants.TotallyAboutConstants;
import in.totallyabout.dao.CelebrityDAO;
import in.totallyabout.dto.TrendingCelebrityDTO;
import in.totallyabout.entity.ApplicationData;
import in.totallyabout.entity.CelebrityInfo;
import in.totallyabout.entity.CelebrityUniqueNews;
import in.totallyabout.entity.FacebookInfo;
import in.totallyabout.entity.NewsIds;
import in.totallyabout.entity.TwitterInfo;
import in.totallyabout.redis.RedisHelper;
import in.totallyabout.utility.HttpNewsClient;

@RestController
public class CelebrityReadController {

	@Autowired
	private MongoTemplate mongoTemplate;

	@Autowired
	private RedisTemplate<String, Object> redisTemplate;
	@Autowired
	private RedisHelper redisHelper;
	
	@Autowired
	HttpNewsClient httpClient;
	public final static Logger logger = Logger.getLogger(CelebrityReadController.class);


	@RequestMapping(method = RequestMethod.PUT, value = "/celebrity/invalidurls", produces = "application/json")
	@ResponseBody
	public HashMap<String, String> findInvalidCelebrityUrls(@RequestBody HashMap<String, String> urls) throws ClientProtocolException, IOException {

		logger.info("In findInvalidCelebrityUrls");
		HashMap<String, String> failedUrls = new HashMap<String, String>();
		int code;
		String url="";
		for (String name : urls.keySet()) {
			try {
				url = urls.get(name);
				code = httpClient.getHttpGetResponseCode(url);
				if (code != 200) {
					logger.info("Found invalid url: " + url);
					failedUrls.put(name, url);
				}
			} catch (Exception e) {
				failedUrls.put(name, url);
				logger.info(e.getMessage());
			}
		}
		return failedUrls;
	}


	@RequestMapping(method = RequestMethod.GET, value = "/celebrity/{id}")
	@ResponseBody
	public CelebrityInfo getCelebrityInfoById(@PathVariable("id") String id) {
		logger.info("getCelebrityInfoById " + id);
		logger.info("in getCelebrityInfoById id: " + id);
		String key = "celebrity:"+id;
		if(redisHelper.hasKey(key)) {
			return redisHelper.getValue(key, CelebrityInfo.class);
		}
		CelebrityInfo celebrityInfo = mongoTemplate.findOne(Query.query(Criteria.where("_id").is(id)),
				CelebrityInfo.class, TotallyAboutConstants.CELEBRITY_INFO);
		if (celebrityInfo.getVideosetIds() != null) {
			List<String> yidsformatted = new ArrayList<String>();
			List<String> list = celebrityInfo.getVideosetIds().get("youtube");
			for (String str : list) {
				yidsformatted.add(str);
			}
			HashMap<String, List<String>> map = new HashMap<String, List<String>>();
			map.put("youtube", yidsformatted);
			celebrityInfo.setVideosetIds(map);
		}
		return redisHelper.saveRedisKey(key,celebrityInfo,TotallyAboutConstants.CELEBRITY_INFO_CACHE_TTL);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/celebrity/all")
	@ResponseBody
	public List<CelebrityInfo> getAllCelebrityInfo() {
		logger.info("in getCelebrityInfoById");
		List<CelebrityInfo> celebrityInfolist = mongoTemplate.findAll(CelebrityInfo.class);
		if (celebrityInfolist != null) {

			for (int i = 0; i < celebrityInfolist.size(); i++) {
				if (celebrityInfolist.get(i).getVideosetIds() != null) {
					List<String> yidsformatted = new ArrayList<String>();
					List<String> list = celebrityInfolist.get(i).getVideosetIds().get("youtube");
					for (String str : list) {
						yidsformatted.add(str);
					}
					HashMap<String, List<String>> map = new HashMap<String, List<String>>();
					map.put("youtube", yidsformatted);
					celebrityInfolist.get(i).setVideosetIds(map);
				}
			}
		}
		return celebrityInfolist;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/celebrity/social")
	@ResponseBody
	public List<CelebrityInfo> getCelebrityInfo() {
		Query q = new Query();
		q.fields().include("socialMappingIds");

		List<CelebrityInfo> celebrityInfo = mongoTemplate.find(q, CelebrityInfo.class);
		return celebrityInfo;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/celebrity/searchTerms/{id}")
	@ResponseBody
	public String getCelebritySearchTerms(@PathVariable("id") String id) {
		Query q = new Query(Criteria.where("_id").is(id));
		q.fields().include("searchTerms");

		List<CelebrityInfo> celebrityInfo = mongoTemplate.find(q, CelebrityInfo.class);
		return celebrityInfo.get(0).getSearchTerms();
	}

	@RequestMapping(method = RequestMethod.GET, value = "/celebrity/displayName/{id}")
	@ResponseBody
	public String getCelebrityDisplayName(@PathVariable("id") String id) {
		Query q = new Query(Criteria.where("_id").is(id));
		q.fields().include("displayName");

		List<CelebrityInfo> celebrityInfo = mongoTemplate.find(q, CelebrityInfo.class);
		return celebrityInfo.get(0).getDisplayName();
	}

	@RequestMapping(method = RequestMethod.GET, value = "/celebrity/twitter")
	@ResponseBody
	public List<TwitterInfo> getCelebrityTwitterInfoById(@RequestParam("ids") List<String> ids) {
		logger.info("in getCelebrityTwitterInfoById "); // ,
		List<TwitterInfo> twitterInfo = mongoTemplate.find(Query.query(Criteria.where("_id").in(ids)),
				TwitterInfo.class, TotallyAboutConstants.TWITTER_INFO);
		return twitterInfo;
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/celebrity/twitter/createUpdate", produces = "application/json")
	@ResponseBody
	public void createUpdateCelebrityTwitter(@RequestBody List<TwitterInfo> twitterInfo) {
		for (TwitterInfo t : twitterInfo) {
			if (t.getId() != null && t.getId() != "null" && !StringUtils.isEmpty(t.getId()))
				mongoTemplate.save(t);// ;.upsert(Query.query(Criteria.where("id").is(t.getId())),
			// Update.update(t.getId(), t),
			// TwitterInfo.class);
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/celebrity/facebook", produces = "application/json")
	@ResponseBody
	public List<FacebookInfo> getCelebrityFacebookInfoById(@RequestParam("ids") List<String> ids) {
		logger.info("In getCelebrityFacebookInfoById"); // ,
		List<FacebookInfo> fbInfo = mongoTemplate.find(Query.query(Criteria.where("_id").in(ids)), FacebookInfo.class,
				TotallyAboutConstants.FACEBOOK_INFO);
		return fbInfo;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/celebrity/facebook/all", produces = "application/json")
	@ResponseBody
	public List<FacebookInfo> getCelebrityFacebookInfoById() {
		logger.info("In getCelebrityFacebookInfoById"); // ,
		List<FacebookInfo> fbInfo = mongoTemplate.findAll(FacebookInfo.class);
		return fbInfo;
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/celebrity/facebook/createUpdate", produces = "application/json")
	@ResponseBody
	public void createUpdateCelebrityFacebook(@RequestBody List<FacebookInfo> fbInfo) {
		logger.info("In createUpdateCelebrityFacebook"); // ,

		for (FacebookInfo t : fbInfo) {
			if (t.getId() != null && t.getId() != "null" && !StringUtils.isEmpty(t.getId()))
				mongoTemplate.save(t);
			// ;.upsert(Query.query(Criteria.where("id").is(t.getId())),
			// Update.update(t.getId(), t), TwitterInfo.class);
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/celebrity/fetchTrendingCeleb", produces = "application/json")
	@ResponseBody
	public List<TrendingCelebrityDTO> getTrendingCelebrities() {
		ValueOperations<String, Object> ops = this.redisTemplate.opsForValue();
		if (redisTemplate.hasKey("trendingCeleb")) {
			logger.info("trending celeb found in cache");
			return (List<TrendingCelebrityDTO>) ops.get("trendingCeleb");
		}

		ApplicationData applicationData = mongoTemplate.findOne(
				Query.query(Criteria.where("_id").is("trendingCelebrities")), ApplicationData.class, TotallyAboutConstants.APPLICATION_DATA);
		List<String> celebrityIds = Arrays.asList(applicationData.getValue().split(","));
		List<CelebrityInfo> celebrityInfoList = mongoTemplate.find(Query.query(Criteria.where("_id").in(celebrityIds)),
				CelebrityInfo.class, TotallyAboutConstants.CELEBRITY_INFO);

		List<TrendingCelebrityDTO> trendingCelebrityList = new ArrayList<TrendingCelebrityDTO>();
		for (CelebrityInfo celebrityInfo : celebrityInfoList) {
			TrendingCelebrityDTO trendingCelebrityDTO = new TrendingCelebrityDTO();
			trendingCelebrityDTO.setDisplayName(celebrityInfo.getDisplayName());
			trendingCelebrityDTO.setId(celebrityInfo.getId());
			trendingCelebrityDTO.setImageUrl(celebrityInfo.getImageUrl());
			trendingCelebrityList.add(trendingCelebrityDTO);
		}
		ops.set("trendingCeleb", trendingCelebrityList);
		this.redisTemplate.expire("trendingCeleb", 1, TimeUnit.MINUTES);
		return trendingCelebrityList;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/celebrity/fetchTrendingVideos", produces = "application/json")
	@ResponseBody
	public List<String> getTrendingVideos() {
		ApplicationData applicationData = mongoTemplate.findOne(Query.query(Criteria.where("_id").is("trendingVideos")),
				ApplicationData.class, TotallyAboutConstants.APPLICATION_DATA);
		List<String> videoIds = Arrays.asList(applicationData.getValue().split(","));
		return videoIds;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/applicationData/fetchLastPostedAt", produces = "application/json")
	@ResponseBody
	public String fetchLastPostedAtProcessed() {
		String lastPostedAtprocessed = null;
		ApplicationData applicationData = mongoTemplate.findOne(Query.query(Criteria.where("_id").is("lastPostedAtProcessed")),
				ApplicationData.class, TotallyAboutConstants.APPLICATION_DATA);

		if(applicationData.getId() !=null)
			lastPostedAtprocessed = applicationData.getValue();

		return lastPostedAtprocessed;
	}

	@RequestMapping(method = RequestMethod.POST, value = "/applicationData/LastPostedAt/update", produces = "application/json")
	@ResponseBody
	public void updateLastPostedAtProcessed(@RequestBody ApplicationData applicationData) {
		try{
			mongoTemplate.save(applicationData);
		}
		catch(Exception e){
			logger.info("Update failed. No document found with id: "+applicationData.getId());
			logger.info(e.getMessage());
		}
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/applicationData/create", produces = "application/json")
	@ResponseBody
	public void createApplicationData(@RequestBody ApplicationData applicationData) {
		if(applicationData.getId() !=null)
			mongoTemplate.insert(applicationData, TotallyAboutConstants.APPLICATION_DATA);
		else
			logger.info("applicationData id is: "+applicationData.getId());
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/celebrityUniqueNews/create", produces = "application/json")
	@ResponseBody
	public void createCelebrityUniqueNews(@RequestBody CelebrityUniqueNews celebrityUniqueNews) {
		logger.info("In create CelebrityUniqueNews");
		try{
			if(StringUtils.isNotBlank(celebrityUniqueNews.get_id()))
				mongoTemplate.insert(celebrityUniqueNews, TotallyAboutConstants.CELEBRITY_UNIQUE_NEWS);
		}catch(Exception e){
			logger.info("Failed to add CelebrityUniqueNews"+e.getMessage());
		}
	}

	@RequestMapping(method = RequestMethod.PUT, value = "/celebrityUniqueNews/bulkcreate", produces = "application/json")
	@ResponseBody
	public void createBulkCelebrityUniqueNews(@RequestBody List<CelebrityUniqueNews> CelebrityUniqueNewsList) {
		logger.info("In createBulkCelebrityUniqueNews");
		if (CelebrityUniqueNewsList.size() > 0) {
			for(CelebrityUniqueNews CelebrityUniqueNews:CelebrityUniqueNewsList)
				try {
					if (StringUtils.isNotBlank(CelebrityUniqueNews.get_id()))
						mongoTemplate.insert(CelebrityUniqueNews, TotallyAboutConstants.CELEBRITY_UNIQUE_NEWS);
				} catch (Exception e) {
					logger.info("Failed to add CelebrityUniqueNews"+e.getMessage());
				}
		}else{
			logger.info("received CelebrityUniqueNewsList size is less than 0");
		}
	}

	@RequestMapping(method = RequestMethod.POST, value = "/celebrityUniqueNews/bulkupdate", produces = "application/json")
	@ResponseBody
	public void updateBulkCelebrityUniqueNews(@RequestBody List<CelebrityUniqueNews> celebrityUniqueNewsList) {
		logger.info("In update BulkCelebrityUniqueNews");
		List<CelebrityUniqueNews> updatedCelebrityUniqueNewsList = new ArrayList<CelebrityUniqueNews>();
		CelebrityUniqueNews updatedCelebrityUniqueNews = new CelebrityUniqueNews();
		List<NewsIds> updatedNewsIdsList = new ArrayList<NewsIds>();
		String celebId;
		// Set<CelebrityUniqueNews> updatedCelebrityUniqueNewsSet=null;

		if (celebrityUniqueNewsList.size() > 0) {
			for (int i = 0; i < celebrityUniqueNewsList.size(); i++) {
				CelebrityUniqueNews newCelebrityUniqueNews = celebrityUniqueNewsList.get(i);
				celebId = newCelebrityUniqueNews.get_id();
				CelebrityUniqueNews currentCelebrityUniqueNews = mongoTemplate.findOne(
						Query.query(Criteria.where("_id").is(celebId)), CelebrityUniqueNews.class,
						TotallyAboutConstants.CELEBRITY_UNIQUE_NEWS);
				if (currentCelebrityUniqueNews != null) {
					List<NewsIds> currentNewsIdsList = currentCelebrityUniqueNews.getNewsIds();

					// To merge two lists
					for (NewsIds newNewsIds : newCelebrityUniqueNews.getNewsIds()) {
						Boolean isExists = false;
						for (NewsIds currentNewsIds : currentNewsIdsList) {
							if (currentNewsIds.getNewsId().equals(newNewsIds.getNewsId()))
								isExists = true;
						}
						if (!isExists)
							currentNewsIdsList.add(newNewsIds);
					}

					// updatedNewsIdsList.addAll(newCelebrityUniqueNews.getNewsIds());
					updatedCelebrityUniqueNews.set_id(celebId);
					updatedCelebrityUniqueNews.setNewsIds(currentNewsIdsList);
					updatedCelebrityUniqueNewsList.add(updatedCelebrityUniqueNews);
				}else{
					updatedCelebrityUniqueNewsList.add(celebrityUniqueNewsList.get(i));
				}

				if (updatedCelebrityUniqueNewsList != null && updatedCelebrityUniqueNewsList.size() > 0) {
					for (CelebrityUniqueNews updatedCelebrityUniNews : updatedCelebrityUniqueNewsList)
						if (StringUtils.isNotBlank(updatedCelebrityUniNews.get_id()))
							mongoTemplate.save(updatedCelebrityUniNews, TotallyAboutConstants.CELEBRITY_UNIQUE_NEWS);
				}
			}
		} else {
			logger.info("received CelebrityUniqueNewsList size is less than 0");
		}
	}

	@RequestMapping(method = RequestMethod.GET, value = "/celebrityUniqueNews/{id}", produces = "application/json")
	@ResponseBody
	public CelebrityUniqueNews getCelebrityUniqueNewsById(@PathVariable String id) {
		CelebrityUniqueNews celebrityUniqueNews = null;
		if (id != null) {
			try {
				celebrityUniqueNews = CelebrityDAO.getCelebrityUniqueNewsById(id);
			} catch (Exception e) {
				logger.info("Exception while fetching getCelebrityUniqueNews with id " + id + e.getMessage());
			}
		}
		return celebrityUniqueNews;
	}


	@RequestMapping(method = RequestMethod.GET, value = "/celebrityUniqueNews/all")
	@ResponseBody
	public List<CelebrityUniqueNews> getAllCelebrityUniqueNews() {
		logger.info("in getAllCelebrityUniqueNews");
		List<CelebrityUniqueNews> celebrityInfolist = mongoTemplate.findAll(CelebrityUniqueNews.class);
		return celebrityInfolist;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/celebrityUniqueNews")
	@ResponseBody
	public List<CelebrityUniqueNews> getAllCelebrityUniqueNewsByPostedAt(@RequestParam Long from,@RequestParam Long to) {
		logger.info("in getAllCelebrityUniqueNewsByPostedAt");

		List<CelebrityUniqueNews> celebrityInfolist = mongoTemplate.findAll(CelebrityUniqueNews.class);
		List<CelebrityUniqueNews> celebrityUniqueNewsList = new ArrayList<CelebrityUniqueNews>();

		if(from==null || to == null){
			logger.info("Invalid from/to value in the request param");
			return null;
		}
		if (celebrityInfolist.size() > 0) {
			for (CelebrityUniqueNews celebrityAllUniqueNews : celebrityInfolist) {
				List<NewsIds> newsIds = new ArrayList<NewsIds>();
				for(NewsIds newsId: celebrityAllUniqueNews.getNewsIds()){
					if(newsId.getPostedAt()>=from && newsId.getPostedAt()<=to)
						newsIds.add(newsId);
				}
				CelebrityUniqueNews celebrityUniqueNews = new CelebrityUniqueNews();
				celebrityUniqueNews.set_id(celebrityAllUniqueNews.get_id());
				celebrityUniqueNews.setNewsIds(newsIds);
				celebrityUniqueNewsList.add(celebrityUniqueNews);
			}
		}
		return celebrityUniqueNewsList;
	}
}