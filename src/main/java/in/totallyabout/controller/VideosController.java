package in.totallyabout.controller;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;

import in.totallyabout.constants.Url;
import in.totallyabout.dto.VideosDTO;
import in.totallyabout.utility.HttpNewsClient;

@RestController
public class VideosController {
	@Autowired
	HttpNewsClient httpClient;


	@RequestMapping(method = RequestMethod.PUT, value = "/videos/bulkcreate")
	@ResponseBody
	public String createBulkVideos(@RequestBody List<VideosDTO> bulkVideosDTO) {
		// System.out.println(bulkNewsDTO.toString());

		String response = "failed";
		for (int i = 0; i < bulkVideosDTO.size(); i++) {
			System.out.println(bulkVideosDTO.get(i).toString());
			response = httpClient.putVideos(bulkVideosDTO.get(i));
		}
		return response;
	}

	@RequestMapping(method = RequestMethod.GET, value = "/videos/{celebrityId}")
	@ResponseBody
	public List<VideosDTO> getViedos(@PathVariable String celebrityId, @RequestParam(required = false) Integer page,
			@RequestParam(required = false) Integer pageSize) throws UnsupportedEncodingException {
		if (page == null)
			page = 1;
		if (pageSize == null)
			pageSize = 12;
		int from = (page - 1) * pageSize;
		int size = pageSize;

		String url = Url.ES + "/videos/_search?from=" + from + "&size=" + size + "&q="
				+ URLEncoder.encode("celebId:" + celebrityId, "UTF-8");
		System.out.println(url);
		List<VideosDTO> res = new ArrayList<VideosDTO>();
		try {

			res = httpClient.getVideosResponse(url);
		} catch (JsonProcessingException e) {

			e.printStackTrace();
		} catch (IOException e) {

			e.printStackTrace();
		}
		return res;

	}

}
