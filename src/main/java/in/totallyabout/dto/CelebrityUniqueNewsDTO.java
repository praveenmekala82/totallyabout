package in.totallyabout.dto;
import java.util.List;

public class CelebrityUniqueNewsDTO {

	private String _id;
	private List<NewsIds> newsIds;
	
	public String get_id() {
		return _id;
	}
	public void set_id(String _id) {
		this._id = _id;
	}
	public List<NewsIds> getNewsIds() {
		return newsIds;
	}
	public void setNewsIds(List<NewsIds> newsIds) {
		this.newsIds = newsIds;
	}
}