package in.totallyabout.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class ResponseObjectDTO<T> {

	private boolean error = false;
	private String errorMessage = "";
	private String errorType;
	private String objectType = null;
	private T data = null;
	

	public ResponseObjectDTO() {
	}

	public ResponseObjectDTO(String errorType, String errorMessage) {
		this.error = true;
		this.errorType = errorType;
		this.errorMessage = errorMessage;
	}

	/**
	 * @param data
	 * @param error
	 * @param errorMessage
	 */

	public boolean isError() {
		return error;
	}

	public void setError(boolean error) {
		this.error = error;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
		if (data != null)
			this.objectType = data.getClass().getCanonicalName();
		else
			this.objectType = null;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public String getErrorType() {
		return errorType;
	}

	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}

	public String getObjectType() {
		return objectType;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ResponseObjectDTO [error=");
		builder.append(error);
		builder.append(", errorMessage=");
		builder.append(errorMessage);
		builder.append(", errorType=");
		builder.append(errorType);
		builder.append(", objectType=");
		builder.append(objectType);
		builder.append(", data=");
		builder.append(data);
		builder.append("]");
		return builder.toString();
	}
	
}
