package in.totallyabout.dto;

import java.io.Serializable;

public class NewsDTO implements Serializable{
	private String handleId;
	private String handleSource;
	private String handleType;
	private String newsId;
	private String title;
	private String celebrityId;
	private String description;
	private Long postedAt;
	private Integer handleLevelPopularity;
	private String profilePic;
	private String newsImage;
	private String newsVideo;
	
	public String getNewsImage() {
		return newsImage;
	}
	public void setNewsImage(String newsImage) {
		this.newsImage = newsImage;
	}
	public String getNewsVideo() {
		return newsVideo;
	}
	public void setNewsVideo(String newsVideo) {
		this.newsVideo = newsVideo;
	}
	public String getProfilePic() {
		return profilePic;
	}
	public void setProfilePic(String profilePic) {
		this.profilePic = profilePic;
	}
	public Long getPostedAt() {
		return postedAt;
	}
	public void setPostedAt(Long postedAt) {
		this.postedAt = postedAt;
	}
	public Integer getHandleLevelPopularity() {
		return handleLevelPopularity;
	}
	public void setHandleLevelPopularity(Integer handleLevelPopularity) {
		this.handleLevelPopularity = handleLevelPopularity;
	}
	
	public String getHandleId() {
		return handleId;
	}
	public void setHandleId(String handleId) {
		this.handleId = handleId;
	}
	public String getHandleType() {
		return handleType;
	}
	public void setHandleType(String handleType) {
		this.handleType = handleType;
	}

	
	public String getHandleSource() {
		return handleSource;
	}
	public void setHandleSource(String handleSource) {
		this.handleSource = handleSource;
	}
	public String getNewsId() {
		return newsId;
	}
	public void setNewsId(String newsId) {
		this.newsId = newsId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getCelebrityId() {
		return celebrityId;
	}
	public void setCelebrityId(String celebrityId) {
		this.celebrityId = celebrityId;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	@Override
	public String toString() {
		return "NewsDTO [handleId=" + handleId + ", profilePic=" + profilePic + ", handleSource=" + handleSource + ", handleType=" + handleType
				+ ", newsId=" + newsId + ", title=" + title + ", celebrityId=" + celebrityId + ", newsVideo=" + newsVideo +" , newsImage= " + newsImage + ", description="
				+ description + ", postedAt=" + postedAt + ", handleLevelPopularity=" + handleLevelPopularity + "]";
	}
	
	
	
}
