package in.totallyabout.dto;

public class NewsIds {

	private String newsId;
	private Long postedAt;
	
	public String getNewsId() {
		return newsId;
	}
	public void setNewsId(String newsId) {
		this.newsId = newsId;
	}
	public Long getPostedAt() {
		return postedAt;
	}
	public void setPostedAt(Long postedAt) {
		this.postedAt = postedAt;
	}
}
