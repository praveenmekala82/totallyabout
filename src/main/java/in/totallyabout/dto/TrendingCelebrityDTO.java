package in.totallyabout.dto;

import java.io.Serializable;

public class TrendingCelebrityDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4511607480467826181L;
	private String id;
	private String displayName;
	private String imageUrl;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
}
