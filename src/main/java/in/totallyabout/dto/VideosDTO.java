package in.totallyabout.dto;

public class VideosDTO {

	String celebId;
	String vidoeId;
	String likeCount;
	String viewCount;
	String commentCount;
	String dislikeCount;
	String description;
	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Long getPublishedAt() {
		return publishedAt;
	}

	public void setPublishedAt(Long publishedAt) {
		this.publishedAt = publishedAt;
	}

	public String getDefaultThumbnailUrl() {
		return defaultThumbnailUrl;
	}

	public void setDefaultThumbnailUrl(String defaultThumbnailUrl) {
		this.defaultThumbnailUrl = defaultThumbnailUrl;
	}

	public String getMediumThumbnailUrl() {
		return mediumThumbnailUrl;
	}

	public void setMediumThumbnailUrl(String mediumThumbnailUrl) {
		this.mediumThumbnailUrl = mediumThumbnailUrl;
	}

	String title;
	Long publishedAt;
	String defaultThumbnailUrl;
	String mediumThumbnailUrl;

	public String getCelebId() {
		return celebId;
	}

	public void setCelebId(String celebId) {
		this.celebId = celebId;
	}

	public String getVidoeId() {
		return vidoeId;
	}

	public void setVidoeId(String vidoeId) {
		this.vidoeId = vidoeId;
	}

	public String getLikeCount() {
		return likeCount;
	}

	public void setLikeCount(String likeCount) {
		this.likeCount = likeCount;
	}

	public String getViewCount() {
		return viewCount;
	}

	public void setViewCount(String viewCount) {
		this.viewCount = viewCount;
	}

	public String getCommentCount() {
		return commentCount;
	}

	public void setCommentCount(String commentCount) {
		this.commentCount = commentCount;
	}

	public String getDislikeCount() {
		return dislikeCount;
	}

	public void setDislikeCount(String dislikeCount) {
		this.dislikeCount = dislikeCount;
	}


}
