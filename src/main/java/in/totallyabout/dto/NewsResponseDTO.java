package in.totallyabout.dto;

import java.io.Serializable;
import java.util.List;

public class NewsResponseDTO implements Serializable {
	List<NewsDTO> direct;
	List<NewsDTO> indirect;
	public List<NewsDTO> getDirect() {
		return direct;
	}
	public void setDirect(List<NewsDTO> direct) {
		this.direct = direct;
	}
	public List<NewsDTO> getIndirect() {
		return indirect;
	}
	public void setIndirect(List<NewsDTO> indirect) {
		this.indirect = indirect;
	}
	
	
}
