package in.totallyabout.constants;

public class TotallyAboutConstants {
	
	public  enum TotallyAboutError{
		
		
		USERNAME_DOESNOT_EXIST(USERNAME_DOESNOT_EXIST_MSG,INVALID_RECORD_CODE),
		USERNAME_EXISTS(USERNAME_ALREADY_EXISTS,PARAMETER_VALIDATION_CODE),
		USERID_DOESNOT_EXISTS(USERID_DOESNOT_EXISTS_MSG,PARAMETER_VALIDATION_CODE);
	
		
		private String errorMessage;

		private String errorCode;
	
		private TotallyAboutError(String errorMessage,String errorCode)
		{
			this.errorCode=errorCode;
			this.errorMessage=errorMessage;
		}
		
		public String getErrorMessage() {
			return errorMessage;
		}
		
		public void setErrorMessage(String msg) {
			this.errorMessage=msg;
		}
		public String getErrorCode() {
			return errorCode;
		}
	}
	
	public static final String USERNAME_DOESNOT_EXIST_MSG="cannot find a user with the give user name";
	public static final String USERNAME_ALREADY_EXISTS="UserName already exists";
	public static final String USERID_DOESNOT_EXISTS_MSG="UserID does not  exist";
	
	public static final String INVALID_RECORD_CODE="404";
	public static final String PARAMETER_VALIDATION_CODE = "400";
	public static final String MONGO_REQUEST_INFO = "requestInfo";
	public static final String CELEBRITY_INFO = "celebrityInfo";
	public static final String APPLICATION_DATA = "applicationData";
	public static final String TWITTER_INFO = "twitterInfo";
	public static final String FACEBOOK_INFO = "facebookInfo";
	public static final String CELEBRITY_UNIQUE_NEWS = "celebrityUniqueNews";
	
	//ttl in minutes
	public static final Integer CELEBRITY_INFO_CACHE_TTL = 5;
	public static final Integer NEWS_CACHE_TTL = 1;
	
	
	
}
