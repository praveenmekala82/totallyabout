package in.totallyabout.redis;

import java.util.concurrent.TimeUnit;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Service;

@Service
public class RedisHelper {

	@Autowired
	private RedisTemplate<String, Object> redisTemplate;
	private ValueOperations<String, Object> ops;

	@PostConstruct
	private void init() {
		ops = this.redisTemplate.opsForValue();
	}
	
	public <T> T saveRedisKey(String key, T t, int ttlInMinutes) {
		ops.set(key, t);
		redisTemplate.expire(key, ttlInMinutes, TimeUnit.MINUTES);
		return t;
	}
	
	public boolean hasKey(String key) {
		return redisTemplate.hasKey(key);
	}
	
	@SuppressWarnings("unchecked")
	public <T> T getValue(String key, Class<T> clazz) {
		return (T)ops.get(key);
	}
}
