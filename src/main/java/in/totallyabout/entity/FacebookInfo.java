package in.totallyabout.entity;

import org.springframework.data.annotation.Id;

public class FacebookInfo {
	@Id
	private String _id;
	private String since;
	public String getId() {
		return _id;
	}
	public void setId(String _id) {
		this._id = _id;
	}
	public String getSince() {
		return since;
	}
	public void setSince(String since) {
		this.since = since;
	}
	
	
}
