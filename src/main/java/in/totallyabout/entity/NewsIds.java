package in.totallyabout.entity;

public class NewsIds {

	private String newsId;
	private Long postedAt;
	
	public String getNewsId() {
		return newsId;
	}
	public void setNewsId(String newsId) {
		this.newsId = newsId;
	}
	public Long getPostedAt() {
		return postedAt;
	}
	public void setPostedAt(Long postedAt) {
		this.postedAt = postedAt;
	}
	
	@Override
	public boolean equals(Object o) {
		return ((NewsIds)o).getNewsId().equals(this.getNewsId());
	}
	@Override
	public int hashCode() {
		return this.getNewsId().hashCode();
	}
}
