package in.totallyabout.entity;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;

import org.springframework.data.annotation.Id;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class CelebrityInfo  implements Serializable {
    
	@Id
	private String _id;
	private String displayName;
	private HashMap< String, String> basicInfo;
	private HashMap< String, String> achievements;
	private HashMap< String, String> socialMappingIds;
	private String searchTerms;
	private HashMap< String, String> videoIds;
	private HashMap< String, List<String>> videosetIds;
	private String extract;
	private String imageUrl;
	private String category;
	
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	public HashMap<String, String> getVideoIds() {
		return videoIds;
	}
	public void setVideoIds(HashMap<String, String> videoIds) {
		this.videoIds = videoIds;
	}
	public String getExtract() {
		return extract;
	}
	public void setExtract(String extract) {
		this.extract = extract;
	}
	public HashMap<String, String> getSocialMappingIds() {
		return socialMappingIds;
	}
	public void setSocialMappingIds(HashMap<String, String> socialMappingIds) {
		this.socialMappingIds = socialMappingIds;
	}
	
	public String getId() {
		return _id;
	}
	public void setId(String _id) {
		this._id = _id;
	}
	public String getDisplayName() {
		return displayName;
	}
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	public HashMap<String, String> getBasicInfo() {
		return basicInfo;
	}
	public void setBasicInfo(HashMap<String, String> basicInfo) {
		this.basicInfo = basicInfo;
	}
	public HashMap<String, String> getAchievements() {
		return achievements;
	}
	public void setAchievements(HashMap<String, String> achievements) {
		this.achievements = achievements;
	}
	
	
	public String getSearchTerms() {
		return searchTerms;
	}
	public void setSearchTerms(String searchTerms) {
		this.searchTerms = searchTerms;
	}
	public String toString(){
		return "_id: "+_id+"displayName: "+displayName+"basicInfo: "+basicInfo==null? "":basicInfo.toString()+"petNames: "+searchTerms+"achievements: "+achievements+"socialMappingIds: "+socialMappingIds==null?"":socialMappingIds.toString()+" imageUrl: "+imageUrl;
	}
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public HashMap<String, List<String>> getVideosetIds() {
		return videosetIds;
	}
	public void setVideosetIds(HashMap<String, List<String>> videosetIds) {
		this.videosetIds = videosetIds;
	}
	
}
