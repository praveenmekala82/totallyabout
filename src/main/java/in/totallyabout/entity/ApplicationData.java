package in.totallyabout.entity;

import org.springframework.data.annotation.Id;

public class ApplicationData {
	
	@Id
	private String _id;
	private String value;
	
	public String getId() {
		return _id;
	}
	public void setId(String _id) {
		this._id = _id;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
}
