package in.totallyabout.entity;

import org.springframework.data.annotation.Id;

public class TwitterInfo {
	@Id
	private String _id;
	private String sinceId;
	public String getId() {
		return _id;
	}
	public void setId(String _id) {
		this._id = _id;
	}
	public String getSinceId() {
		return sinceId;
	}
	public void setSinceId(String sinceId) {
		this.sinceId = sinceId;
	}
	
	
}
