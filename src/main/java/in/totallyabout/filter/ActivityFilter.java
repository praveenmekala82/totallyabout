package in.totallyabout.filter;

import java.io.IOException;
import java.util.StringTokenizer;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;

import in.totallyabout.constants.TotallyAboutConstants;
import in.totallyabout.entity.RequestInfo;

public class ActivityFilter implements Filter {

	public final static Logger logger = Logger.getLogger(ActivityFilter.class);
	@Autowired
	private MongoTemplate mongoTemplate;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    	SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this,
                filterConfig.getServletContext());
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
    	HttpServletRequest request = (HttpServletRequest) servletRequest;
    	HttpServletResponse response = (HttpServletResponse) servletResponse;
    	String ipAddress = getClientIpAddress(request);
    	RequestInfo ipAddressInfo = mongoTemplate.findOne(Query.query(Criteria.where("_id").is(ipAddress)),
    			RequestInfo.class, TotallyAboutConstants.MONGO_REQUEST_INFO);
    	if(ipAddressInfo == null) {
    		logger.info("ip address not found");
    		ipAddressInfo = new RequestInfo();
    		ipAddressInfo.set_id(ipAddress);
    		ipAddressInfo.setCount(1);
    		mongoTemplate.insert(ipAddressInfo, TotallyAboutConstants.MONGO_REQUEST_INFO);
    	} else {
    		
			ipAddressInfo.setCount(ipAddressInfo.getCount() + 1);
			mongoTemplate.save(ipAddressInfo);
    	}
			filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {

    }
    
    public static String getClientIpAddress(HttpServletRequest request) {
        String xForwardedForHeader = request.getHeader("X-Forwarded-For");
        if (xForwardedForHeader == null) {
            return request.getRemoteAddr();
        } else {
            // As of https://en.wikipedia.org/wiki/X-Forwarded-For
            // The general format of the field is: X-Forwarded-For: client, proxy1, proxy2 ...
            // we only want the client
            return new StringTokenizer(xForwardedForHeader, ",").nextToken().trim();
        }
    }
}