package in.totallyabout.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.web.context.support.SpringBeanAutowiringSupport;

import in.totallyabout.constants.TotallyAboutConstants;
import in.totallyabout.controller.CelebrityReadController;
import in.totallyabout.entity.RequestInfo;

public class CorsFilter implements Filter {

	public final static Logger logger = Logger.getLogger(CorsFilter.class);
	@Autowired
	private MongoTemplate mongoTemplate;
    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    	SpringBeanAutowiringSupport.processInjectionBasedOnServletContext(this,
                filterConfig.getServletContext());
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
    	HttpServletRequest request = (HttpServletRequest) servletRequest;
    	HttpServletResponse response = (HttpServletResponse) servletResponse;
    	String origin = request.getHeader("Origin");
    	String auth = request.getHeader("auth");
    	//logger.info("origin is: " + origin);
    	
		//if("admin".equals(auth) || ((origin!=null && (origin.contains("localhost") || origin.contains("127.0.0.1") || origin.contains("totallyabout.in"))))) {
    	if(true) {
			//logger.info("allowed origin or admin");
			
	        response.setHeader("Access-Control-Allow-Origin", origin);
	        response.setHeader("Access-Control-Allow-Credentials", "true");
	        response.setHeader("Access-Control-Allow-Methods", "POST, GET, HEAD, OPTIONS");
	        response.setHeader("Access-Control-Allow-Headers", "Origin, Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers");
	        response.setStatus(200);
	        filterChain.doFilter(servletRequest, servletResponse);
	        RequestInfo originInfo = mongoTemplate.findOne(Query.query(Criteria.where("_id").is("origin:"+origin)),
	    			RequestInfo.class, TotallyAboutConstants.MONGO_REQUEST_INFO);
	    	if(originInfo == null) {
	    		logger.info("origin not found");
	    		originInfo = new RequestInfo();
	    		originInfo.set_id("origin:"+origin);
	    		originInfo.setCount(1);
	    		mongoTemplate.insert(originInfo, TotallyAboutConstants.MONGO_REQUEST_INFO);
	    	} else {
	    		
	    		originInfo.setCount(originInfo.getCount() + 1);
				mongoTemplate.save(originInfo);
	    	}
		} else {
			 response.setStatus(415);
		}
    }

    @Override
    public void destroy() {

    }
}