package in.totallyabout.utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.log4j.Logger;
import org.eclipse.jetty.util.StringUtil;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import in.totallyabout.constants.Url;
import in.totallyabout.controller.CelebrityReadController;
import in.totallyabout.dto.CelebrityNewsDTO;

@Service
public class CelebrityNewsUtility {
	public final static Logger logger = Logger.getLogger(CelebrityNewsUtility.class);

	private MultiThreadedHttpConnectionManager connectionManager;
	private HttpClient client;
	

	@PostConstruct
	private void init() {
		connectionManager = new MultiThreadedHttpConnectionManager();
		logger.info("total connections: " + connectionManager.getParams().getMaxTotalConnections());
		connectionManager.getParams().setMaxTotalConnections(100);
		logger.info("total connections after setting: " + connectionManager.getParams().getMaxTotalConnections());
		client = new HttpClient(connectionManager);
	}
	
	public List<CelebrityNewsDTO> getTopSearchNewsByCategory( String category, String id ) throws UnsupportedEncodingException{
		String url = Url.ES+"/celebrity/_search?q=";;
		if(StringUtil.isNotBlank(category) && !"undefined#undefined".equals(category)) {
			url = url + URLEncoder.encode("category:"+category+"*","UTF-8");
			if(category.contains("#")) {
				String[] cats = category.split("#");
				url = url + URLEncoder.encode(" OR category:"+cats[0]+"*", "UTF-8");
			}
			url = url + "&";
		}
		url = url + URLEncoder.encode("sort=rating:asc&pretty","UTF-8");
		System.out.println(url);
		String line = "";
		StringBuffer res = new StringBuffer();
		GetMethod request = new GetMethod(url);
		try {
			client.executeMethod(request);
			BufferedReader rd = new BufferedReader(new InputStreamReader(request.getResponseBodyAsStream()));
			while ((line = rd.readLine()) != null) {
				//System.out.println(line);
				res.append(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {

			request.releaseConnection();
		}

		JsonParser parser = new JsonParser();
		JsonObject json = (JsonObject) parser.parse(res.toString());
		JsonObject obj = json.get("hits").getAsJsonObject();

		JsonArray jarray = obj.get("hits").getAsJsonArray();
		List<CelebrityNewsDTO> celebrityNews = new ArrayList<CelebrityNewsDTO>();

		for (int i = 0; i < jarray.size(); i++) {

			CelebrityNewsDTO celebrityNewsDTO = new CelebrityNewsDTO();

			JsonObject ids = jarray.get(i).getAsJsonObject();
			JsonObject source = ids.get("_source").getAsJsonObject();
			celebrityNewsDTO = new Gson().fromJson(source.toString(), CelebrityNewsDTO.class);

			//celebrityNewsDTO.setCategory(category);
			celebrityNewsDTO.setId(ids.get("_id").getAsString());
			if(!celebrityNewsDTO.getId().equals(id)) {
				celebrityNews.add(celebrityNewsDTO);
			}

		}
          System.out.println(celebrityNews.toString());
		
		return celebrityNews;

	}
	
	public MultiThreadedHttpConnectionManager getConnectionManager() {
		return connectionManager;
	}

	public void setConnectionManager(MultiThreadedHttpConnectionManager connectionManager) {
		this.connectionManager = connectionManager;
	}

	public HttpClient getClient() {
		return client;
	}

	public void setClient(HttpClient client) {
		this.client = client;
	}

}
