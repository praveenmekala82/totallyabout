package in.totallyabout.utility;

import java.util.NoSuchElementException;

import javax.naming.OperationNotSupportedException;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import com.google.gson.Gson;

import in.totallyabout.dto.ResponseObjectDTO;


@ControllerAdvice
public class GlobalExceptionHandler {
	
	private static Logger LOGGER = LoggerFactory.getLogger(GlobalExceptionHandler.class);
	
	/*@ExceptionHandler(RestException.class)
	@ResponseBody
	public String handleError(RestException e, HttpServletResponse response) {	
		Gson gson = new Gson();
		LOGGER.warn("Error in totallyAbout, code: " + e.getErrorCode() + ", message: " + e.getErrorMessage() );
		response.setStatus(Integer.parseInt(e.getErrorCode()));
		return gson.toJson(e);
	}*/
	
	
	/*@ExceptionHandler(NoSuchElementException.class)
	@ResponseBody
	public String handleError(NoSuchElementException e, HttpServletResponse response) {	
		Gson gson = new Gson();
		LOGGER.warn("Error in totallyAbout, code: " + 404 + ", message: " + e);
		response.setStatus(404);
		return gson.toJson(new ResponseObjectDTO(CoreServiceError.NO_SUCH_PRODUCT_FOUND.getErrorCode(),CoreServiceError.NO_SUCH_PRODUCT_FOUND.getErrorMessage()));
	} */
	
	@ExceptionHandler(TotallyAboutException.class)
	@ResponseBody
	public String handleError(TotallyAboutException e, HttpServletResponse response) {	
		e.printStackTrace();
		Gson gson = new Gson();
		LOGGER.warn("Error in totallyAbout, code: " + e.getErrorCode() + ", message: " + e.getErrorMessage() );
		if(e.getErrorCode() != null){
			response.setStatus(Integer.parseInt(e.getErrorCode()));
		}
		else{
			
			response.setStatus(HttpStatus.INTERNAL_SERVER_ERROR.value());
		}
		ResponseObjectDTO responseObjectDTO = new ResponseObjectDTO(e.getErrorCode(),e.getErrorMessage());
		if(e.getData() != null){
			responseObjectDTO.setData(e.getData());
		}
		String errorJson = gson.toJson(responseObjectDTO);
		LOGGER.warn("error json:"+ errorJson);
		return errorJson;
	}

	@ExceptionHandler(OperationNotSupportedException.class)
	@ResponseBody
	public String handleError(OperationNotSupportedException e, HttpServletResponse response) {
		Gson gson = new Gson();
		response.setStatus(415);
		LOGGER.error("GENERAL error in totallyAbout",e);
		return gson.toJson(new ResponseObjectDTO("415",e.getMessage()));
	}
	
	@ExceptionHandler(IllegalArgumentException.class)
	@ResponseBody
	public String handleError(IllegalArgumentException e, HttpServletResponse response) {
		Gson gson = new Gson();
		response.setStatus(400);
		LOGGER.error("GENERAL error in totallyAbout",e);
		return gson.toJson(new ResponseObjectDTO("400",e.getMessage()));
	}
	
	@ExceptionHandler(Exception.class)
	@ResponseBody
	public String handleError(Exception e, HttpServletResponse response) {
		e.printStackTrace();
		Gson gson = new Gson();
		response.setStatus(500);
		LOGGER.error("GENERAL error in totallyAbout",e);
		return gson.toJson(new ResponseObjectDTO("500",e.getMessage()));
	}

}
