package in.totallyabout.utility;

import in.totallyabout.dto.CelebrityInfoRequestDTO;
import in.totallyabout.entity.CelebrityInfo;

public class DTOPopulator {

	public static CelebrityInfo celebrityDTOPopulator(CelebrityInfoRequestDTO celebrityInfoRequestDTO){
		
		CelebrityInfo celebrityInfo= new CelebrityInfo();
		celebrityInfo.setId(celebrityInfoRequestDTO.getId());
		celebrityInfo.setDisplayName(celebrityInfoRequestDTO.getDisplayName());
		celebrityInfo.setBasicInfo(celebrityInfoRequestDTO.getBasicInfo());
		celebrityInfo.setAchievements(celebrityInfoRequestDTO.getAchievements());
		celebrityInfo.setSocialMappingIds(celebrityInfoRequestDTO.getSocialMappingIds());
		celebrityInfo.setSearchTerms(celebrityInfoRequestDTO.getSearchTerms());
		celebrityInfo.setVideoIds(celebrityInfoRequestDTO.getVideoIds());
		celebrityInfo.setVideosetIds(celebrityInfoRequestDTO.getVideosetIds());
		celebrityInfo.setExtract(celebrityInfoRequestDTO.getExtract());
		celebrityInfo.setImageUrl(celebrityInfoRequestDTO.getImageUrl());
		celebrityInfo.setCategory(celebrityInfoRequestDTO.getCategory());
		
		return celebrityInfo;
	}
}
