package in.totallyabout.utility;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import javax.annotation.PostConstruct;

import org.apache.commons.httpclient.HttpClient;
import org.apache.commons.httpclient.MultiThreadedHttpConnectionManager;
import org.apache.commons.httpclient.methods.GetMethod;
import org.apache.commons.httpclient.methods.PostMethod;
import org.apache.commons.httpclient.methods.PutMethod;
import org.apache.commons.httpclient.methods.StringRequestEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import in.totallyabout.constants.Url;
import in.totallyabout.dto.CelebAutocompleteDTO;
import in.totallyabout.dto.ImagesDTO;
import in.totallyabout.dto.NewsDTO;
import in.totallyabout.dto.VideosDTO;
import in.totallyabout.entity.CelebrityInfo;

@Service
public class HttpNewsClient {
	public final static Logger logger = Logger.getLogger(HttpNewsClient.class);

	private MultiThreadedHttpConnectionManager connectionManager;
	private HttpClient client;
	

	@PostConstruct
	private void init() {
		connectionManager = new MultiThreadedHttpConnectionManager();
		logger.info("total connections: " + connectionManager.getParams().getMaxTotalConnections());
		connectionManager.getParams().setMaxTotalConnections(100);
		logger.info("total connections after setting: " + connectionManager.getParams().getMaxTotalConnections());
		client = new HttpClient(connectionManager);
	}

	public String putNews(NewsDTO newsDTO) {
		StringBuffer res = new StringBuffer();
		String handleSource = newsDTO.getHandleSource().toString();
		String newsId = "";
		//newsDTO.getNewsId().toString();
		if (StringUtils.isEmpty(newsId)) {
			newsId = UUID.randomUUID().toString().replaceAll("-", "");
		}
		//String url = Url.ES + "/news/" + handleSource + "/" + newsId + "?pretty";
		String url = Url.ES + "/news/handleNews/"+newsId;
		logger.info("calling url " + url);
		PutMethod httpPost = new PutMethod(url);
		try {

			JSONObject jsonObj = new JSONObject();
			// jsonObj.put("source",newsDTO.getSource() );
			// jsonObj.put("newsId", newsDTO.getNewsId());
			jsonObj.put("title", newsDTO.getTitle());
			//jsonObj.put("id", newsDTO.getNewsId());
			jsonObj.put("celebrityId", newsDTO.getCelebrityId());
			jsonObj.put("description", newsDTO.getDescription());
			jsonObj.put("handleType", newsDTO.getHandleType());
			jsonObj.put("handleSource", handleSource);
			jsonObj.put("handleId", newsDTO.getHandleId());
			jsonObj.put("postedAt", newsDTO.getPostedAt());
			jsonObj.put("handleLevelPopularity", newsDTO.getHandleLevelPopularity());
			jsonObj.put("profilePic", newsDTO.getProfilePic());
			jsonObj.put("newsImage", newsDTO.getNewsImage());
			jsonObj.put("newsVideo", newsDTO.getNewsVideo());

			// HttpPut httpPut = new HttpPut(url);
			StringRequestEntity entity = new StringRequestEntity(jsonObj.toString(), "application/json", HTTP.UTF_8);
			httpPost.setRequestEntity(entity);
			logger.info("calling client");
			client.executeMethod(httpPost);

			BufferedReader rd = new BufferedReader(new InputStreamReader(httpPost.getResponseBodyAsStream()));
			String line = "";
			while ((line = rd.readLine()) != null) {
				// System.out.println("**********"+line+"*********");
				res = res.append(line);
			}

		}

		catch (Exception e) {
			e.printStackTrace();
		} finally {
			httpPost.releaseConnection();
		}
		logger.info(res.toString());
		return res.toString();
	}

	public String putImages(ImagesDTO imagesDTO) {
		StringBuffer res = new StringBuffer();
		String imageId = imagesDTO.getImageId();
		if (StringUtils.isEmpty(imageId)) {
			imageId = UUID.randomUUID().toString().replaceAll("-", "");
		}
		String url = Url.ES + "/images/~/" + imageId + "?pretty";
		PutMethod httpPut = new PutMethod(url);

		try {

			JSONObject jsonObj = new JSONObject();
			// jsonObj.put("source",newsDTO.getSource() );
			// jsonObj.put("newsId", newsDTO.getNewsId());
			jsonObj.put("title", imagesDTO.getTitle());
			jsonObj.put("celebId", imagesDTO.getCelebId());
			jsonObj.put("imageId", imagesDTO.getImageId());
			jsonObj.put("compUrl", imagesDTO.getCompUrl());
			jsonObj.put("previewUrl", imagesDTO.getPreviewUrl());
			jsonObj.put("thumbnailUrl", imagesDTO.getThumbnailUrl());
			StringRequestEntity entity = new StringRequestEntity(jsonObj.toString(), "application/json", HTTP.UTF_8);
			httpPut.setRequestEntity(entity);
			client.executeMethod(httpPut);
			BufferedReader rd = new BufferedReader(new InputStreamReader(httpPut.getResponseBodyAsStream()));
			String line = "";
			while ((line = rd.readLine()) != null) {
				// System.out.println("**********"+line+"*********");
				res = res.append(line);
			}

		}

		catch (Exception e) {
			e.printStackTrace();
		} finally {
			httpPut.releaseConnection();
		}
		return res.toString();
	}

	public String putESCelebData(CelebrityInfo ci, Integer rating) {
		StringBuffer res = new StringBuffer();
		String id = ci.getId();
		String url = Url.ES + "/celebrity/~/" + id + "?pretty";
		PutMethod httpPut = new PutMethod(url);
		try {

			JSONObject jsonObj = new JSONObject();
			// jsonObj.put("source",newsDTO.getSource() );
			// jsonObj.put("newsId", newsDTO.getNewsId());
			jsonObj.put("name", ci.getDisplayName());
			int r = 10;
			if (rating != null) {
				r = rating;
			}
			jsonObj.put("rating", r);
			String category = ci.getCategory();
			if (category == null) {
				category = "";
			}
			jsonObj.put("category", category);

			StringRequestEntity entity = new StringRequestEntity(jsonObj.toString(), "application/json", HTTP.UTF_8);
			httpPut.setRequestEntity(entity);
			client.executeMethod(httpPut);

			BufferedReader rd = new BufferedReader(new InputStreamReader(httpPut.getResponseBodyAsStream()));
			String line = "";
			while ((line = rd.readLine()) != null) {
				// System.out.println("**********"+line+"*********");
				res = res.append(line);
			}

		}

		catch (Exception e) {
			e.printStackTrace();
		} finally {
			httpPut.releaseConnection();
		}
		return res.toString();
	}

	public String getNews(String url) {
		String line = "";
		StringBuffer res = new StringBuffer();
		GetMethod request = new GetMethod(url);
		try {

			client.executeMethod(request);
			BufferedReader rd = new BufferedReader(new InputStreamReader(request.getResponseBodyAsStream()));
			while ((line = rd.readLine()) != null) {
				res.append(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			request.releaseConnection();
		}
		return res.toString();
	}
	
	public List<NewsDTO> getNewsbyRequestBody(String url,String requestBody) {
		String line = "";
		StringBuffer res = new StringBuffer();
		GetMethod request = new GetMethod(url);
		List<NewsDTO> news = new ArrayList<NewsDTO>();

		try {
			CloseableHttpClient closableClient = HttpClients.createDefault();
			HttpPost httpPost = new HttpPost(url);
		    StringEntity entity = new StringEntity(requestBody);
		    httpPost.setEntity(entity);
		    httpPost.setHeader("Accept", "application/json");
		    httpPost.setHeader("Content-type", "application/json");
			//client.executeMethod(request);
		    CloseableHttpResponse response = closableClient.execute(httpPost);
			BufferedReader rd = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
			while ((line = rd.readLine()) != null) {
				res.append(line);
			}
			
			JsonParser parser = new JsonParser();
			JsonObject json = (JsonObject) parser.parse(res.toString());
			JsonObject obj = json.get("hits").getAsJsonObject();

			JsonArray jarray = obj.get("hits").getAsJsonArray();

			for (int i = 0; i < jarray.size(); i++) {

				NewsDTO newsdto = new NewsDTO();

				JsonObject ids = jarray.get(i).getAsJsonObject();
				JsonObject source = ids.get("_source").getAsJsonObject();
				newsdto = new Gson().fromJson(source.toString(), NewsDTO.class);

				newsdto.setHandleSource(ids.get("_type").getAsString());
				newsdto.setNewsId(ids.get("_id").getAsString());

				news.add(newsdto);

			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			request.releaseConnection();
		}
		return  news;

	}

	public List<NewsDTO> getNewsResponse(String url) throws JsonProcessingException, IOException {
		String line = "";
		StringBuffer res = new StringBuffer();
		GetMethod request = new GetMethod(url);
		try {

			client.executeMethod(request);
			BufferedReader rd = new BufferedReader(new InputStreamReader(request.getResponseBodyAsStream()));
			while ((line = rd.readLine()) != null) {
				res.append(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			request.releaseConnection();
		}

		JsonParser parser = new JsonParser();
		System.out.println(res.toString());
		JsonObject json = (JsonObject) parser.parse(res.toString());
		JsonObject obj = json.get("hits").getAsJsonObject();

		JsonArray jarray = obj.get("hits").getAsJsonArray();
		List<NewsDTO> news = new ArrayList<NewsDTO>();

		for (int i = 0; i < jarray.size(); i++) {

			NewsDTO newsdto = new NewsDTO();

			JsonObject ids = jarray.get(i).getAsJsonObject();
			JsonObject source = ids.get("_source").getAsJsonObject();
			newsdto = new Gson().fromJson(source.toString(), NewsDTO.class);

			newsdto.setHandleSource(ids.get("_type").getAsString());
			newsdto.setNewsId(ids.get("_id").getAsString());

			news.add(newsdto);

		}

		return news;
	}

	public List<ImagesDTO> getImagesResponse(String url) throws JsonProcessingException, IOException {
		String line = "";
		StringBuffer res = new StringBuffer();
		GetMethod request = new GetMethod(url);
		try {
			client.executeMethod(request);
			BufferedReader rd = new BufferedReader(new InputStreamReader(request.getResponseBodyAsStream()));
			while ((line = rd.readLine()) != null) {
				res.append(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			request.releaseConnection();
		}

		JsonParser parser = new JsonParser();
		JsonObject json = (JsonObject) parser.parse(res.toString());
		JsonObject obj = json.get("hits").getAsJsonObject();

		JsonArray jarray = obj.get("hits").getAsJsonArray();
		List<ImagesDTO> images = new ArrayList<ImagesDTO>();

		for (int i = 0; i < jarray.size(); i++) {

			ImagesDTO img = new ImagesDTO();

			JsonObject ids = jarray.get(i).getAsJsonObject();
			JsonObject source = ids.get("_source").getAsJsonObject();
			img = new Gson().fromJson(source.toString(), ImagesDTO.class);
			img.setImageId(ids.get("_id").getAsString());

			images.add(img);

		}

		return images;
	}

	public List<CelebAutocompleteDTO> getAutocompleteResponse(String url) {
		String line = "";
		StringBuffer res = new StringBuffer();
		GetMethod request = new GetMethod(url);
		try {
			client.executeMethod(request);
			BufferedReader rd = new BufferedReader(new InputStreamReader(request.getResponseBodyAsStream()));
			while ((line = rd.readLine()) != null) {
				res.append(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			request.releaseConnection();
		}

		JsonParser parser = new JsonParser();
		JsonObject json = (JsonObject) parser.parse(res.toString());
		JsonObject obj = json.get("hits").getAsJsonObject();

		JsonArray jarray = obj.get("hits").getAsJsonArray();
		List<CelebAutocompleteDTO> results = new ArrayList<CelebAutocompleteDTO>();

		for (int i = 0; i < jarray.size(); i++) {

			CelebAutocompleteDTO result = new CelebAutocompleteDTO();

			JsonObject ids = jarray.get(i).getAsJsonObject();
			JsonObject source = ids.get("_source").getAsJsonObject();
			result = new Gson().fromJson(source.toString(), CelebAutocompleteDTO.class);

			result.setId(ids.get("_id").getAsString());

			results.add(result);

		}
		return results;
	}

	public String putVideos(VideosDTO vidoesDTO) {
		StringBuffer res = new StringBuffer();
		String videoId = vidoesDTO.getVidoeId();
		if (StringUtils.isEmpty(videoId)) {
			videoId = UUID.randomUUID().toString().replaceAll("-", "");
		}
		String url = Url.ES + "/videos/~/" + videoId + "?pretty";
		PutMethod putMethod = new PutMethod(url);
		try {

			JSONObject jsonObj = new JSONObject();
			jsonObj.put("celebId", vidoesDTO.getCelebId());
			jsonObj.put("videoId", vidoesDTO.getVidoeId());
			jsonObj.put("commentCount", vidoesDTO.getCommentCount());
			jsonObj.put("dislikeCount", vidoesDTO.getDislikeCount());
			jsonObj.put("likeCount", vidoesDTO.getLikeCount());
			jsonObj.put("viewCount", vidoesDTO.getViewCount());
			jsonObj.put("publishedAt", vidoesDTO.getPublishedAt());
			jsonObj.put("defaultThumbnailUrl", vidoesDTO.getDefaultThumbnailUrl());
			jsonObj.put("description", vidoesDTO.getDescription());
			jsonObj.put("mediumThumbnailUrl", vidoesDTO.getMediumThumbnailUrl());
			jsonObj.put("title", vidoesDTO.getTitle());
			HttpPut httpPut = new HttpPut(url);
			StringEntity entity = new StringEntity(jsonObj.toString(), HTTP.UTF_8);
			entity.setContentType("application/json");
			httpPut.setEntity(entity);
			client.executeMethod(putMethod);
			BufferedReader rd = new BufferedReader(new InputStreamReader(putMethod.getResponseBodyAsStream()));
			String line = "";
			while ((line = rd.readLine()) != null) {
				// System.out.println("**********"+line+"*********");
				res = res.append(line);
			}

		}

		catch (Exception e) {
			e.printStackTrace();
		} finally {

			putMethod.releaseConnection();
		}
		return res.toString();
	}

	public List<VideosDTO> getVideosResponse(String url) throws JsonProcessingException, IOException {
		String line = "";
		StringBuffer res = new StringBuffer();
		GetMethod request = new GetMethod(url);
		try {
			client.executeMethod(request);
			BufferedReader rd = new BufferedReader(new InputStreamReader(request.getResponseBodyAsStream()));
			while ((line = rd.readLine()) != null) {
				res.append(line);
			}

		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			request.releaseConnection();
		}

		JsonParser parser = new JsonParser();
		JsonObject json = (JsonObject) parser.parse(res.toString());
		JsonObject obj = json.get("hits").getAsJsonObject();

		JsonArray jarray = obj.get("hits").getAsJsonArray();
		List<VideosDTO> videos = new ArrayList<VideosDTO>();

		for (int i = 0; i < jarray.size(); i++) {

			VideosDTO vid = new VideosDTO();

			JsonObject ids = jarray.get(i).getAsJsonObject();
			JsonObject source = ids.get("_source").getAsJsonObject();
			vid = new Gson().fromJson(source.toString(), VideosDTO.class);
			vid.setVidoeId(ids.get("_id").getAsString());

			videos.add(vid);

		}

		return videos;
	}
	
	public int getHttpGetResponseCode(String url) throws ClientProtocolException, IOException{
		
		int code=0;

		CloseableHttpClient client = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(url);
		HttpResponse response = client.execute(request);
		
		if(response !=null)
			code = response.getStatusLine().getStatusCode();
		
		if(response !=null && code!=200)
		   System.out.println("Failed url response: "+EntityUtils.toString(response.getEntity()));
		
		return code;
	}
	
	public MultiThreadedHttpConnectionManager getConnectionManager() {
		return connectionManager;
	}

	public void setConnectionManager(MultiThreadedHttpConnectionManager connectionManager) {
		this.connectionManager = connectionManager;
	}

	public HttpClient getClient() {
		return client;
	}

	public void setClient(HttpClient client) {
		this.client = client;
	}

}
