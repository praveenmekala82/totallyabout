package in.totallyabout.utility;


public class TotallyAboutException extends Exception {

	private static final long serialVersionUID = 1L;
	private String errorCode;
	private String errorMessage;
	private Object data;
	
	public TotallyAboutException(final String s) {
		super(s);
	}

	public TotallyAboutException(final Throwable throwable) {
		super(throwable);
	}

	public TotallyAboutException(final String s, final Throwable throwable) {
		super(s, throwable);
	}

	public TotallyAboutException(final String code, final String message) {
		super(message);
		this.errorCode = code;
		this.errorMessage = message;
	}

	public TotallyAboutException(TotallyAboutException totallyAboutError) {

		super(totallyAboutError.getErrorMessage());
		this.errorCode = totallyAboutError.getErrorCode();
		this.errorMessage = totallyAboutError.getErrorMessage();
	}

	public String getErrorCode() {
		return errorCode;
	}

	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}

	public String getErrorMessage() {
		return errorMessage;
	}

	public void setErrorMessage(String errorMessage) {
		this.errorMessage = errorMessage;
	}

	public Object getData() {
		return data;
	}

	public void setData(Object data) {
		this.data = data;
	}
}
