package in.totallyabout.dao;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;

import in.totallyabout.entity.CelebrityUniqueNews;
import in.totallyabout.utility.HttpNewsClient;

@Repository
@Component
public class CelebrityDAO {
	
	@Autowired
	private static MongoTemplate mongoTemplate;
	@Autowired
	HttpNewsClient httpClient;	
	public static MongoTemplate getMongoTemplate() {
		return mongoTemplate;
	}

	public static void setMongoTemplate(MongoTemplate mongoTemplate) {
		CelebrityDAO.mongoTemplate = mongoTemplate;
	}

	public static final String CELEBRITY_UNIQUE_NEWS = "celebrityUniqueNews";


	public final static Logger logger = Logger.getLogger(CelebrityDAO.class);
	
	@Transactional
	public static void createCelebrityUniqueNews(CelebrityUniqueNews celebrityUniqueNews) {
		try{
		if(StringUtils.isNotBlank(celebrityUniqueNews.get_id())){
			CelebrityUniqueNews celebUniqueNews = getCelebrityUniqueNewsById(celebrityUniqueNews.get_id());
			if(celebUniqueNews ==null || celebUniqueNews.getNewsIds().size()<1)
				mongoTemplate.save(celebrityUniqueNews, CELEBRITY_UNIQUE_NEWS);
			else
				logger.info("Skipping createCelebrityUniqueNews for celeb: "+celebrityUniqueNews.get_id());
		}
		}catch(Exception e){
			logger.info("Failed to add CelebrityUniqueNews"+e.getMessage());
		}
	}
	
	@Transactional
	public static CelebrityUniqueNews getCelebrityUniqueNewsById(@PathVariable String id) {
		CelebrityUniqueNews celebrityUniqueNews = null;
		if (id != null) {
			try {
				celebrityUniqueNews = mongoTemplate.findOne(Query.query(Criteria.where("_id").is(id)), CelebrityUniqueNews.class, CELEBRITY_UNIQUE_NEWS);
			} catch (Exception e) {
				logger.info("Exception while fetching getCelebrityUniqueNews with id " + id + e.getMessage());
			}
		}
		return celebrityUniqueNews;
	}
}
